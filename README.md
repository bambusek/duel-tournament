# Duel laser cut figures and pedestals

This repository contains source files for laser engraved and cut Duel6 figures and their pedestals. Engraving/cutting is tested on Trotec Speedy 300 while using aspen 4mm thick plywood for figures and 6mm for pedestals.

## Laser settings - Figure (4mm)

Color |  | Mode | Power | Speed
----- | --- | ---- | ----- | ----- 
Cyan |  ![#00FFFF](https://placehold.it/25/00FFFF/00FFFF) | Engrave | 50 | 20
Red | ![#FF0000](https://placehold.it/25/FF0000/FF0000) | Engrave | 50 | 35
Blue |  ![#0000FF](https://placehold.it/25/0000FF/0000FF) | Engrave | 50 | 60
Green |  ![#00FF00](https://placehold.it/25/00FF00/00FF00) | Engrave | 50 | 80
Orange |  ![#FF6600](https://placehold.it/25/FF6600/FF6600) |Engrave | 50 |100
Magenta | ![#FF00FF](https://placehold.it/25/FF00FF/FF00FF) | Cut | 100 | 0.9

## Laser settings - Pedestal (6mm)

Color |  | Mode | Power | Speed
----- | --- | ---- | ----- | ----- 
Black |  ![#00FFFF](https://placehold.it/25/000000/000000) | Engrave | 50 | 30
Magenta | ![#FF00FF](https://placehold.it/25/FF00FF/FF00FF) | Cut | 100 | 0.65



